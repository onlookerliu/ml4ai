import React from 'react';
import { XHeader, AppContainer, LeftContainer } from '../common/containers';
import { XTop, XMenu, XBottom } from '../common/items';
import { api, ajax } from '../../config.service';
import { MenuComponent } from '../management/menu';
import { message } from 'antd';


class WelCome extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            pages: (<h2 style={{ align: 'center', textAlign: 'center' }}>深度学习与人工智能</h2>)
        };
    }

    menuClick(me, e) {
        console.log(e.item.props.data);
        let component = e.item.props.data.menuData;
        let pages = [];
        switch (component) {
            case "MenuComponent":
                pages.push(<MenuComponent />);
                break;
            default:
                message.info('该功能未开发！');
                break;
        }
        me.setState({ pages: pages });
    }

    render() {
        let pages = this.state.pages;
        return (
            <div className="App" style={{ width: '100%', height: '100%', overflowY: 'hidden' }}>
                <XHeader>
                    <XTop><h2 style={{ textAlign: 'center' }}>机器学习与人工智能 Machine Learning For Artifical Intelligence</h2></XTop>
                    <XBottom>
                        <XMenu properties={{ mode: 'horizontal' }} url={api.menuDataURL} handle={(e) => { this.menuClick(this, e); }} />
                    </XBottom>
                </XHeader>
                <AppContainer>
                    {pages}
                </AppContainer>
            </div>
        );
    }
}

export { WelCome };