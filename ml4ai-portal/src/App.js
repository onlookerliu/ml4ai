import React, { Component } from 'react';
import './App.css';
import RootPage from './components/ui/pages/root.page';
import { BrowserRouter, Route } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="width100height100">
          <Route exact path="/" component={() => { return (<div className="App"><RootPage></RootPage></div>) }}></Route>
          <Route exact path="/hadoop" component={() => { return (<div className="App"><RootPage></RootPage></div>) }}></Route>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
