import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the BaiduPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var BMap;
declare var BMAP_STATUS_SUCCESS;

@IonicPage()
@Component({
  selector: 'page-baidu',
  templateUrl: 'baidu.html',
})
export class BaiduPage {

  @ViewChild("map")
  private mapElement: ElementRef;
  @ViewChild("search")
  private searchElement: ElementRef;

  private map: any = undefined;
  private searchVal = undefined;
  private callback: any = undefined;
  private location: any = {
    x: undefined,
    y: undefined,
    address: undefined
  };
  private searchResult: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private geo: Geolocation) {

  }

  ionViewDidLoad() {

  }

  ionViewDidEnter() {
    console.log("加载地图!");
    this.location = this.navParams.get("location");
    this.callback = this.navParams.get("callback");

    let map = this.map = new BMap.Map(this.mapElement.nativeElement, { enableMapClick: true });
    map.enableScrollWheelZoom();
    map.enableContinuousZoom();

    let point = new BMap.Point(104.16, 30.67);
    let marker = new BMap.Marker(point);
    if (this.location && this.location.x && this.location.y) {
      point = new BMap.Point(this.location.x, this.location.y);
      marker = new BMap.Marker(point);
    }
    map.centerAndZoom(point, 16);
    map.clearOverlays();
    map.addOverlay(marker);
    
    try {
      /**
      this.geo.getCurrentPosition().then((resp) => {
        console.log(resp.coords.longitude, resp.coords.latitude);
        let point = new BMap.Point(resp.coords.longitude, resp.coords.latitude);
        if (this.location && this.location.x && this.location.y) {
          point = new BMap.Point(this.location.x, this.location.y);
        }
        map.centerAndZoom(point, 16);
      });
      */
    } catch (e) {

    }
  }

  onSearch(event) {
    let me = this;
    let val = this.searchVal;
    if (val && val.length && val.length > 0) {
      let local;
      var options = {
        onSearchComplete: function (results) {
          me.searchResult = [];
          if (local.getStatus() == BMAP_STATUS_SUCCESS) {
            for (var i = 0; i < results.getCurrentNumPois(); i++) {
              let poi = results.getPoi(i);
              me.searchResult.push({ title: poi.title + ", " + poi.address, data: poi });
            }
          }
        }
      };
      local = new BMap.LocalSearch(this.map, options);
      local.search(val);
    } else {
      this.searchResult = [];
    }
  }

  selectResult(item) {
    console.log(item);
    this.searchResult = [];
    let point = new BMap.Point(item.point.lng, item.point.lat);
    let marker = new BMap.Marker(point);
    this.map.clearOverlays();
    this.map.addOverlay(marker);
    this.map.centerAndZoom(point, 16);
    this.location = {
      x: item.point.lng,
      y: item.point.lat,
      address: item.title + "," + item.address
    };
    if (this.callback) {
      this.callback(this.location);
    }
  }

}
