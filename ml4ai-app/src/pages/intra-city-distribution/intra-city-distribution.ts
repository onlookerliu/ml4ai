import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { NetProvider } from '../../providers/net/net';
import { GlobalsProvider } from '../../providers/globals/globals';
import { PublishListPage } from '../publish-list/publish-list';
import { LoginPage } from '../login/login';
import { Geolocation } from '@ionic-native/geolocation';
import { BaiduPage } from '../baidu/baidu';

/**
 * Generated class for the IntraCityDistributionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-intra-city-distribution',
  templateUrl: 'intra-city-distribution.html',
})
export class IntraCityDistributionPage {

  private intraCityDistributionTask: any = {
    id: undefined,
    businessCode: undefined,
    businessTitle: "新建同城配送",
    businessType: 'IntraCityDistribution',
    businessTypeName: undefined,
    commodityName: undefined,

    destinationAddress: undefined,
    destinationAddressInfo: undefined,
    destinationLink: undefined,
    destinationTelephone: undefined,
    destinationX: undefined,
    destinationY: undefined,

    intraCityDistributionStatus: undefined,

    amount: undefined,

    sourceAddress: undefined,
    sourceLink: undefined,
    sourceTelephone: undefined,
    sourceX: undefined,
    sourceY: undefined,
    sourceAddressInfo: undefined,

    startTime: undefined
  };
  private parent: PublishListPage;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private net: NetProvider,
    private globals: GlobalsProvider,
    private geo: Geolocation,
    private alert: AlertController
  ) {

  }

  ionViewDidLoad() {

  }

  ionViewDidEnter() {
    let intraCityDistributionTask = this.navParams.get("intraCityDistributionTask");
    if (intraCityDistributionTask) {
      for (let key in intraCityDistributionTask) {
        if (intraCityDistributionTask[key]) {
          this.intraCityDistributionTask[key] = intraCityDistributionTask[key];
        }
      }
    }
    this.parent = this.navParams.get("parent");
    this.renderData();
  }

  renderData() {
    let me = this;
    if (this.intraCityDistributionTask.businessType) {
      this.net.postJson(this.globals.urls.enums, { enumname: "BusinessType" }, (response) => {
        if (response && response.code) {
          if (response.code == "200") {
            me.intraCityDistributionTask.businessTypeName = response.data[me.intraCityDistributionTask.businessType];
          } else if (response.code == "401") {
            me.globals.navControl.setRoot(LoginPage);
          }
        }
      }, (error) => {
      });
    }
  }

  selectSource() {
    let me = this;
    this.globals.navControl.push(BaiduPage, {
      location: {
        x: this.intraCityDistributionTask.sourceX,
        y: this.intraCityDistributionTask.sourceY,
        address: this.intraCityDistributionTask.sourceAddressInfo
      },
      callback: (loc) => {
        me.intraCityDistributionTask.sourceX = loc.x;
        me.intraCityDistributionTask.sourceY = loc.y;
        me.intraCityDistributionTask.sourceAddressInfo = loc.address;
      }
    });
  }

  selectDestination() {
    let me = this;
    this.globals.navControl.push(BaiduPage, {
      location: {
        x: this.intraCityDistributionTask.destinationX,
        y: this.intraCityDistributionTask.destinationY,
        address: this.intraCityDistributionTask.destinationAddressInfo
      },
      callback: (loc) => {
        me.intraCityDistributionTask.destinationX = loc.x;
        me.intraCityDistributionTask.destinationY = loc.y;
        me.intraCityDistributionTask.destinationAddressInfo = loc.address;
      }
    });
  }

  save() {
    let me = this;
    let url = this.intraCityDistributionTask.id ? this.globals.urls.saveIntraCityDistribution : this.globals.urls.newIntraCityDistribution;
    this.net.postJson(url, this.intraCityDistributionTask, (response) => {
      if (response && response.code) {
        if (response.code == "200") {
          me.navCtrl.pop();
          me.parent.ionViewDidEnter();
        } else if (response.code == "401") {
          me.globals.navControl.setRoot(LoginPage);
        } else if (response.code == "500") {
          const alert = this.alert.create({
            title: "错误",
            subTitle: response.msg,
            buttons: ['确定']
          });
          alert.present();
        }
      }
    }, (xhr) => {

    });
  }

  delete() {
    let me = this;
    let alert = this.alert.create({
      title: '确认删除',
      message: '你确认要删除吗?',
      buttons: [
        {
          text: '取消',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: '确认',
          handler: () => {
            me.net.postJson(me.globals.urls.deleteIntraCityDistribution, { id: me.intraCityDistributionTask.id }, (response) => {
              if (response && response.code) {
                if (response.code == "200") {
                  me.navCtrl.pop();
                  me.parent.ionViewDidEnter();
                } else if (response.code == "401") {
                  me.globals.navControl.setRoot(LoginPage);
                } else if (response.code == "500") {
                  const a = me.alert.create({
                    title: "错误",
                    subTitle: response.msg,
                    buttons: ['确定']
                  });
                  a.present();
                }
              }
            }, (xhr) => {
            });
          }
        }
      ]
    });
    alert.present();
  }

  publish() {
    let me = this;
    this.net.postJson(this.globals.urls.publishIntraCityDistribution, { id: this.intraCityDistributionTask.id }, (response) => {
      if (response && response.code) {
        if (response.code == "200") {
          me.navCtrl.pop();
          me.parent.ionViewDidEnter();
        } else if (response.code == "401") {
          me.globals.navControl.setRoot(LoginPage);
        } else if (response.code == "500") {
          const a = me.alert.create({
            title: "错误",
            subTitle: response.msg,
            buttons: ['确定']
          });
          a.present();
        }
      }
    }, (xh) => {
    });
  }
}
