import { NgModule } from '@angular/core';
import { EnumSelectDirective } from './enum-select/enum-select';
@NgModule({
	declarations: [EnumSelectDirective],
	imports: [],
	exports: [EnumSelectDirective]
})
export class DirectivesModule {}
