import { Component } from '@angular/core';

/**
 * Generated class for the InfoListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'info-list',
  templateUrl: 'info-list.html'
})
export class InfoListComponent {

  text: string;

  constructor() {
    console.log('Hello InfoListComponent Component');
    this.text = 'Hello World';
  }

}
