package com.ml4ai.backend.service;

/**
 * Created by leecheng on 2018/9/30.
 */
public interface CacheService {

    void put(String hash, String key, String val);

    String get(String hash, String key);

    void delete(String hash, String key);

}
