package com.ml4ai.backend.sys;

import com.ml4ai.backend.service.CacheService;
import com.ml4ai.backend.utils.ObjUtil;
import com.ml4ai.backend.utils.StringHelper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.SessionRepository;

import java.time.Duration;
import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by leecheng on 2017/11/9.
 */
public class ExtSessionRepository implements SessionRepository<ExtSession> {

    @Autowired
    CacheService cacheService;

    @Override
    public ExtSession findById(String id) {
        return getSession(id);
    }

    @Override
    public void deleteById(String id) {
        delete(id);
    }

    @Override
    public ExtSession createSession() {
        String sessionId = StringHelper.uuid();
        Map<String, Object> session = new LinkedHashMap<>();
        long current = System.currentTimeMillis();
        return new ExtSession(sessionId, session, Instant.ofEpochMilli(current), Instant.ofEpochMilli(current), Duration.ofMillis(300));
    }

    @Override
    @SneakyThrows
    public void save(ExtSession session) {
        String val = ObjUtil.obj2str(session);
        cacheService.put("SESSION_STORE", session.getId(), val);
    }

    @SneakyThrows
    public ExtSession getSession(String id) {
        try {
            String val = (String) cacheService.get("SESSION_STORE", id);
            if (val == null) {
                return null;
            } else {
                return (ExtSession) ObjUtil.str2obj(val);
            }
        } catch (Exception e) {
            return null;
        }
    }

    public void delete(String id) {
        cacheService.delete("SESSION_STORE", id);
    }


}
