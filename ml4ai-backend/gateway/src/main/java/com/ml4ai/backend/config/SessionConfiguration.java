package com.ml4ai.backend.config;

import com.ml4ai.backend.sys.ExtSession;
import com.ml4ai.backend.sys.ExtSessionRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.session.SessionRepository;
import org.springframework.session.config.annotation.web.http.EnableSpringHttpSession;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.HttpSessionStrategy;

/**
 * Created by leecheng on 2018/9/30.
 */
@Configuration
@EnableSpringHttpSession
public class SessionConfiguration {

    @Bean
    public SessionRepository<ExtSession> sessionRepository() {
        return new ExtSessionRepository();
    }

    @Bean
    public HttpSessionStrategy pushHttpSessionStrategy() throws Exception {
        HttpSessionStrategy webSessionStrategy;
        HeaderHttpSessionStrategy basedHeaderHttpSessionStrategy = new HeaderHttpSessionStrategy();
        basedHeaderHttpSessionStrategy.setHeaderName("X-AUTH-TOKEN");
        webSessionStrategy = basedHeaderHttpSessionStrategy;
        return webSessionStrategy;
    }
}
