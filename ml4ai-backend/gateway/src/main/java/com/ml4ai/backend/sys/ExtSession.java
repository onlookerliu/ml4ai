package com.ml4ai.backend.sys;

import com.ml4ai.backend.utils.StringHelper;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.session.Session;

import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.Set;

/**
 * Created by leecheng on 2017/11/9.
 */
@Getter
@Setter
@ToString
public class ExtSession implements Session, Serializable {

    private String id;

    private Map<String, Object> data;

    private Instant creationTime;

    private Instant lastAccessedTime;

    private Duration maxInactiveInterval;

    public Map<String, Object> getSessionData() {
        return data;
    }

    @Override
    public String changeSessionId() {
        this.id = StringHelper.uuid();
        return id;
    }

    public ExtSession(String sessionId, Map<String, Object> sessionData, Instant creationTime, Instant lastAccessedTime, Duration maxInactiveInterval) {
        this.id = sessionId;
        this.data = sessionData;
        this.creationTime = creationTime;
        this.lastAccessedTime = lastAccessedTime;
        this.maxInactiveInterval = maxInactiveInterval;
    }


    @Override
    public String getId() {
        return id;
    }

    @Override
    public <T> T getAttribute(String attributeName) {
        return (T) data.get(attributeName);
    }

    @Override
    public Set<String> getAttributeNames() {
        return data.keySet();
    }

    @Override
    public void setAttribute(String attributeName, Object attributeValue) {
        data.put(attributeName, attributeValue);
    }

    @Override
    public void removeAttribute(String attributeName) {
        data.remove(attributeName);
    }

    @Override
    public Instant getCreationTime() {
        return creationTime;
    }

    @Override
    public void setLastAccessedTime(Instant lastAccessedTime) {
        this.lastAccessedTime = lastAccessedTime;
    }

    @Override
    public Instant getLastAccessedTime() {
        return lastAccessedTime;
    }

    @Override
    public void setMaxInactiveInterval(Duration interval) {
        this.maxInactiveInterval = interval;
    }

    @Override
    public Duration getMaxInactiveInterval() {
        return maxInactiveInterval;
    }

    @Override
    public boolean isExpired() {
        return false;
    }
}
