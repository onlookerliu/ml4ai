package com.ml4ai.backend.service.impls;

import com.ml4ai.backend.service.CacheService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by leecheng on 2018/9/30.
 */
@Service
@Transactional
public class CacheServiceImpl implements CacheService {

    private Map<String, Map<String, String>> cache = new LinkedHashMap<>();

    private Map<String, String> attachHash(String hash) {
        Map<String, String> hashVal = cache.get(hash);
        if (hashVal == null) {
            hashVal = new LinkedHashMap<>();
            cache.put(hash, hashVal);
        }
        return hashVal;
    }

    @Override
    public String get(String hash, String key) {
        return attachHash(hash).get(key);
    }

    @Override
    public void put(String hash, String key, String val) {
        attachHash(hash).put(key, val);
    }

    @Override
    public void delete(String hash, String key) {
        attachHash(hash).remove(key);
    }
}
