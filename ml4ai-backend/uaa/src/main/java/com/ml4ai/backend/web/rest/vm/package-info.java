/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ml4ai.backend.web.rest.vm;
