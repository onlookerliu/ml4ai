package com.ml4ai.backend.dto;

import com.ml4ai.backend.dto.base.BaseAuditDTO;
import lombok.Data;

/**
 * Created by uesr on 2018/9/9.
 */
@Data
public class UserDTO extends BaseAuditDTO {

    public final static String SESSION_KEY = "USER_SESSION_TOKEN";

    private String login;

    private String password;

    private String telephone;

    private String nick;

    private String mail;

}
