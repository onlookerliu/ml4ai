package com.ml4ai.backend.dto;

import com.ml4ai.backend.dto.base.BaseAuditDTO;
import com.ml4ai.backend.utils.annotation.QueryColumn;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

/**
 * Created by leecheng on 2018/9/24.
 */
@Data
public class AuthorityDTO extends BaseAuditDTO implements GrantedAuthority {

    private String authority;

    private String authorityName;

    private String authorityData;

    @QueryColumn(propName = "roles.id")
    private Long roleId;
}
