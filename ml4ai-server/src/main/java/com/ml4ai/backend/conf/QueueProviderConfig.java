package com.ml4ai.backend.conf;

import com.ml4ai.backend.consts.Const;
import com.ml4ai.backend.services.boot.QueueService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;

/**
 * Created by uesr on 2018/9/16.
 */
@Configuration
public class QueueProviderConfig {

    @Bean
    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory, MessageListenerAdapter listenerAdapter) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(listenerAdapter, new PatternTopic(Const.PUBLIC_MESSAGE));
        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(QueueService queueService) {
        MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter(queueService, "onMessageArrived");
        messageListenerAdapter.setSerializer(new JdkSerializationRedisSerializer());
        return messageListenerAdapter;
    }

}
