package com.ml4ai.backend.services;

/**
 * Created by uesr on 2018/9/16.
 */
public interface CacheService {

    void put(String hash, String key, Object value);

    Object get(String hash, String key);

    void delete(String hash, String key);

    void push(Object innerMessage);
}
