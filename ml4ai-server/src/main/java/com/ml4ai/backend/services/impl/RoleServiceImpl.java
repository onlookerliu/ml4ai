package com.ml4ai.backend.services.impl;

import com.ml4ai.backend.domain.Role;
import com.ml4ai.backend.dto.RoleDTO;
import com.ml4ai.backend.repository.RoleRepositroy;
import com.ml4ai.backend.services.RoleService;
import com.ml4ai.backend.services.base.impl.BaseServiceImpl;
import com.ml4ai.backend.services.mappers.RoleMapper;
import com.ml4ai.backend.utils.SpringUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.function.Function;

/**
 * Created by leecheng on 2018/9/24.
 */
@Service
@Transactional
public class RoleServiceImpl extends BaseServiceImpl<Role, RoleDTO> implements RoleService {

    @Override
    public Function<Role, RoleDTO> getConvertEntity2DTOFunction() {
        return SpringUtils.getService(RoleMapper.class)::e2d;
    }

    @Override
    public Function<RoleDTO, Role> getConvertDTO2EntityFunction() {
        return SpringUtils.getService(RoleMapper.class)::d2e;
    }

    @Override
    public JpaRepository<Role, Long> getRepository() {
        return SpringUtils.getService(RoleRepositroy.class);
    }
}
