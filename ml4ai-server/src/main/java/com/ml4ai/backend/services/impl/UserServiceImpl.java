package com.ml4ai.backend.services.impl;

import com.ml4ai.backend.domain.User;
import com.ml4ai.backend.dto.UserDTO;
import com.ml4ai.backend.repository.UserRepo;
import com.ml4ai.backend.services.UserService;
import com.ml4ai.backend.services.base.impl.BaseServiceImpl;
import com.ml4ai.backend.services.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.function.Function;

/**
 * Created by uesr on 2018/9/12.
 */
@Service
@Transactional
public class UserServiceImpl extends BaseServiceImpl<User, UserDTO> implements UserService {

    @Autowired
    UserRepo userRepo;

    @Autowired
    UserMapper userMapper;

    @Override
    public Function<User, UserDTO> getConvertEntity2DTOFunction() {
        return userMapper::e2d;
    }

    @Override
    public Function<UserDTO, User> getConvertDTO2EntityFunction() {
        return userMapper::d2e;
    }

    @Override
    public JpaRepository<User, Long> getRepository() {
        return userRepo;
    }

    @Override
    public UserDTO findByLogin(String username) {
        return getConvertEntity2DTOFunction().apply(userRepo.findByLoginAndStatus(username, "1"));
    }
}
