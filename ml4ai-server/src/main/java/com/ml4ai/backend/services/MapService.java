package com.ml4ai.backend.services;

import com.ml4ai.backend.dto.AddInfoDTO;
import com.ml4ai.backend.dto.MapRouteMatrixDTO;

import java.util.Map;

/**
 * 地图服务
 * Created by leecheng on 2017/10/18.
 */
public interface MapService {

    Double[] getLocationByAddress(String address);

    String getLocationAddress(Double lng, Double lat);

    Map<String, Object> getLocation(String address);

    Map<String, Object> getLocation(Double lng, Double lat);

    Map<String, Object> route(MapRouteMatrixDTO routeMatrix);

    AddInfoDTO getLocationX(Double lng, Double lat);

}
