package com.ml4ai.backend.services.impl;

import com.ml4ai.backend.dto.base.PushTaskDTO;
import com.ml4ai.backend.services.CacheService;
import com.ml4ai.backend.services.boot.QueueService;
import com.ml4ai.backend.type.PushType;
import com.ml4ai.backend.utils.WsHost;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

/**
 * Created by uesr on 2018/9/16.
 */
@Slf4j
@Service
public class QueueServiceImpl implements QueueService, CommandLineRunner {

    @Autowired
    CacheService cacheService;

    private final static String PUSH_TO_USER_MESSAGE = "PUSH_TO_USER_MESSAGE";

    private boolean pushEnabled = true;

    @Override
    public void auto() {
        /**
         Runnable ping = () -> {
         while (true) {
         try {
         Thread.sleep(1000L);
         Push2AllMessageDTO push = new Push2AllMessageDTO();
         push.setMessage("欢迎，你好！");
         cacheService.push(push);
         } catch (Exception e) {
         log.debug("{}", e);
         }
         }
         };
         new Thread(ping).start();
         */
    }

    @Override
    public void run(String... args) throws Exception {
        auto();
    }

    @Override
    public void onMessageArrived(Object msg) {
        if (msg != null) {
            PushTaskDTO task = (PushTaskDTO) msg;
            switch (task.getPushType()) {
                case ALL:
                    WsHost.sendOnlineUser(task.getData());
                    break;
                case ASSIGN:

                    break;
                default:
                    cacheService.push(task);
                    break;
            }
        }
    }
}
