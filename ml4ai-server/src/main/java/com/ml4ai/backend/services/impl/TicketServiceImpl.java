package com.ml4ai.backend.services.impl;

import com.ml4ai.backend.domain.Ticket;
import com.ml4ai.backend.dto.TicketDTO;
import com.ml4ai.backend.repository.TicketRepositroy;
import com.ml4ai.backend.services.TicketService;
import com.ml4ai.backend.services.base.impl.BaseServiceImpl;
import com.ml4ai.backend.services.mappers.TicketMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.function.Function;

/**
 * Created by uesr on 2018/9/16.
 */
@Service
@Transactional
public class TicketServiceImpl extends BaseServiceImpl<Ticket, TicketDTO> implements TicketService {

    @Autowired
    TicketMapper ticketMapper;

    @Autowired
    TicketRepositroy ticketRepositroyh;

    @Override
    public Function<Ticket, TicketDTO> getConvertEntity2DTOFunction() {
        return ticketMapper::e2d;
    }

    @Override
    public Function<TicketDTO, Ticket> getConvertDTO2EntityFunction() {
        return ticketMapper::d2e;
    }

    @Override
    public JpaRepository<Ticket, Long> getRepository() {
        return ticketRepositroyh;
    }
}
