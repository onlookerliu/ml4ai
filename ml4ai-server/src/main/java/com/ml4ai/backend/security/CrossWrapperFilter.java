package com.ml4ai.backend.security;

import com.ml4ai.backend.utils.JSONUtil;
import com.ml4ai.backend.utils.RestUtil;
import org.springframework.stereotype.Service;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by leecheng on 2018/1/11.
 */
@Service
public class CrossWrapperFilter implements Filter {

    private ServletContext servletContext;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.servletContext = filterConfig.getServletContext();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain c) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        if (req.getMethod().equalsIgnoreCase("OPTIONS")) {
            res.setHeader("Access-Control-Allow-Origin", req.getHeader("Origin"));
            res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
            res.setHeader("Access-Control-Max-Age", "3600");
            res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, X-Requested-With, Content-Type, Accept,X-AUTH-TOKEN");
            res.setHeader("Access-Control-Allow-Credentials", "true");
            res.setHeader("Content-type", "Application/json;charset=utf8");
            res.getWriter().println(JSONUtil.toJson(RestUtil.success()));
        } else {
            res.setHeader("Access-Control-Allow-Origin", req.getHeader("Origin"));
            res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
            res.setHeader("Access-Control-Max-Age", "3600");
            res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, X-Requested-With, Content-Type, Accept,X-AUTH-TOKEN");
            res.setHeader("Access-Control-Allow-Credentials", "true");
            c.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
