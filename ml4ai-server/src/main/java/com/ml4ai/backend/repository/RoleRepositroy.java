package com.ml4ai.backend.repository;

import com.ml4ai.backend.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by leecheng on 2018/9/24.
 */
public interface RoleRepositroy extends JpaRepository<Role, Long> {
}
