package com.ml4ai.backend.repository;

import com.ml4ai.backend.domain.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by uesr on 2018/9/16.
 */
public interface TicketRepositroy extends JpaRepository<Ticket, Long> {
}
