package com.ml4ai.backend.repository;

import com.ml4ai.backend.domain.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by uesr on 2018/9/9.
 */
public interface AuthorityRepo extends JpaRepository<Authority, Long> {

    @Query("select a from Authority a where a.status = '1'")
    List<Authority> findAvilable();

}
