package com.ml4ai.backend.repository;

import com.ml4ai.backend.domain.Metadata;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by uesr on 2018/9/8.
 */
public interface MetadataRepository extends JpaRepository<Metadata, Long> {

    Metadata findByKey(String key);

}
