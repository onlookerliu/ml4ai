package com.ml4ai.backend.type;

import lombok.Getter;

/**
 * Created by uesr on 2018/9/16.
 */
@Getter
public enum PushType {
    ALL("所有人"),
    ASSIGN("指定");
    private String name;

    private PushType(String name) {
        this.name = name;
    }
}
