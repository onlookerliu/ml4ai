package com.ml4ai.backend.domain.base;

import lombok.Getter;

/**
 * Created by uesr on 2018/9/8.
 */
@Getter
public enum RegistryStatus {

    Submit("已提交"),
    Pass("通过认证"),
    Failure("未通过认证");

    private String name;

    private RegistryStatus(String name) {
        this.name = name;
    }

}
