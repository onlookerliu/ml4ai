package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

/**
 * Created by uesr on 2018/9/8.
 */
@Data
@ToString
@Entity
@Table(name = "T_METADATA_ROW")
public class MetadataRow extends BaseAuditEntity {

    @ManyToOne
    @JoinColumn
    private Metadata metadata;

    @Column(name = "C_key")
    private String key;

    @Column(name = "c_name")
    private String name;

    @Column(name = "c_index")
    private Long index;

}
