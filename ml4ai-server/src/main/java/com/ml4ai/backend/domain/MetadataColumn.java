package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by uesr on 2018/9/8.
 */
@Getter
@Setter
@Entity
@Table(name = "T_METADATA_COLUMN")
public class MetadataColumn extends BaseAuditEntity {

    @ManyToOne
    @JoinColumn(name = "c_metadata_id")
    private Metadata metadata;

    @Column(name = "C_key")
    private String key;

    @Column(name = "c_name")
    private String name;

    @Column(name = "c_index")
    private Integer index;
}
