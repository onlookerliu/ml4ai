package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by uesr on 2018/9/8.
 */
@Getter
@Setter
@Entity
@Table(name = "T_METADATA")
public class Metadata extends BaseAuditEntity {

    @Column(name = "c_name")
    private String name;

    @Column(name = "c_key")
    private String key;

}
