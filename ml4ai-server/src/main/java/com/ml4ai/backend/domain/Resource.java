package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created by uesr on 2018/9/9.
 */
@Getter
@Setter
@Entity
@Table(name = "T_SOURCE")
public class Resource extends BaseAuditEntity {

    @Column
    private String resource;

    @Column(name = "c_name_x")
    private String name;

    @ManyToMany
    @JoinTable(name = "T_AUTH_SRC", joinColumns = @JoinColumn(name = "C_SRC"), inverseJoinColumns = @JoinColumn(name = "C_AUTH"))
    private List<Authority> authorities;
}
